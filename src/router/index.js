
import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import { staticRouter, } from '../utils/data' //引入静态路由
const routes = staticRouter  //静态路由保存
const router = new Router({  //创建路由 
    routes
})
export default router