//静态路由
export const staticRouter = [
    {
        path: "/",
        component: () => import("../views/dr/1.vue"),
        meta: { title: '1', isTitle: true }
    },
    {
        path: "/2",
        meta: { title: '2' },
        component: () => import("../views/dr/2.vue"),
    },
    {
        path: "/3",
        meta: { title: '3' },
        component: () => import("../views/dr/3.vue"),
    },


    {
        path: '*', redirect: '/', meta: { title: '错误', hidden: true },
    }
]

//动态路由
export const dynamicRouter = [
    {
        path: "/4",
        meta: { title: '4' },
        component: 'dr/4.vue',
    },
    {
        path: "/5",
        meta: { title: '5' },
        component: 'dr/5.vue',
    },
    {
        path: "/6",
        meta: { title: '6' },
        component: 'dr/6.vue',
    }
]
