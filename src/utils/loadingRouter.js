import { staticRouter, dynamicRouter } from '../utils/data' //引入静态路由
import router from '../router/index'
import store from '../store/index'
const routes = staticRouter  //静态路由保存
export default function useaddRoute() {
    try {
        // 满足store 有数据   已登录   没有缓存
        if (store.state.is && store.state.routerArr.length === 4) {
            store.dispatch('Nav', staticRouter)
            //模拟数据请求
            setTimeout(() => {
                let data = routesData(dynamicRouter)//将动态路由放放入总路由中
                console.warn(data)
                store.dispatch('Nav', data)// 将路由放置缓存中
                // router.addRoutes(data) //废弃 只是会警告但是还可以使用 数组形式
                data.forEach(item => {
                    router.addRoute(item) //可以单独添加路由格式: /自定义路 ,也可以添加子路由格式：/父路由path/子路由path
                })
            }, 2000)
        } else {
            store.dispatch('Nav', staticRouter)
        }
    } catch (error) {
        console.log(error)
    }

}
function routesData(result) {
    result.forEach(item => {
        routes.push({
            path: item.path,
            name: item.name,
            meta: item.meta,
            component: () => import(`../views/${item.component}`),
        })
    });
    return routes
}