import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const state = {
    is: false,//判断时候登录  登录true  没有false
    routerArr: [] //静态路由+动态路由
}

const mutations = {
    //点击了登录退出
    CHANGESTORE(state, bool) {
        state.is = bool
    },
    //存放路由
    HEARDER(state, data) {
        state.routerArr = data
    },
}
const actions = {
    updateStoreChange(context, state) {
        context.commit('CHANGESTORE', state)
    },
    Nav(context, state) {
        context.commit('HEARDER', state)
    },

}
const getters = {
    updateStore() {
        return state.is
    },
    updateNav() {
        return state.routerArr
    }
}
const store = new Vuex.Store({
    state, mutations, actions, getters
})
export default store